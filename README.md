# Abadonna-vim Extension Project

## About project

Abadonna-vim Extension Project is a fork of spf13-vim. In fact it is spf13-vim on a diet.

I have stripped spf13-vim from many (more than 60%) plugins and configuration settings, leaving only
those I use on regular basis.

## License

The Abadonna-vim Extension Project is licensed under [GPLv2](http://www.gnu.org/licenses/gpl-2.0.html).

### Installation

The easiest way to install abadonna-vim is to use installer by simply copying and pasting the following line into a terminal:

```bash
curl http://gitlab.com/Abadonna/abadonna-vim/raw/master/setup.sh -L -o - | bash
```

### Updating

In order to update project to the latest version, please copy and paste to the terminal:

```bash
cd $HOME/.abadonna-vim && git pull && vim +PlugUpgrade +PlugUpdate +PlugClean +PlugDiff +q && cd -
```

### Plugins

- [junegunn/vim-plug](https://github.com/junegunn/vim-plug)

- [scrooloose/nerdtree](https://github.com/scrooloose/nerdtree)

- [altercation/vim-colors-solarized](https://github.com/altercation/vim-colors-solarized)

- [spf13/vim-colors](https://github.com/spf13/vim-colors)

- [morhetz/gruvbox](https://github.com/morhetz/gruvbox)

- [tpope/vim-surround](https://github.com/tpope/vim-surround)

- [spf13/vim-autoclose](https://github.com/spf13/vim-autoclose)

- [tpope/vim-repeat](https://github.com/tpope/vim-repeat)

- [ctrlpvim/ctrlp.vim](https://github.com/kien/ctrlp.vim)

- [tacahiroy/ctrlp-funky](https://github.com/tacahiroy/ctrlp-funky)

- [kristijanhusak/vim-multiple-cursors](https://github.com/kristijanhusak/vim-multiple-cursors/)

- [matchit.zip](http://www.vim.org/scripts/script.php?script_id=39)

- [bling/vim-airline](https://github.com/bling/vim-airline)

- [bling/vim-bufferline](https://github.com/bling/vim-bufferline)

- [justinmk/vim-sneak](https://github.com/justinmk/vim-sneak)

- [flazz/vim-colorschemes](https://github.com/flazz/vim-colorschemes)

- [mbbill/undotree](https://github.com/mbbill/undotree)

- [mhinz/vim-signify](https://github.com/mhinz/vim-signify)

- [osyo-manga/vim-over](https://github.com/osyo-manga/vim-over)

- [gcmt/wildfire.vim](https://github.com/gcmt/wildfire.vim)

- [mileszs/ack.vim](https://github.com/mileszs/ack.vim)

- [vim-scripts/nerdtree-ack](http://www.vim.org/scripts/script.php?script_id=3878)

- [scrooloose/syntastic](https://github.com/scrooloose/syntastic)

- [tpope/vim-fugitive](https://github.com/tpope/vim-fugitive)

- [scrooloose/nerdcommenter](https://github.com/scrooloose/nerdcommenter)

- [godlygeek/tabular](https://github.com/godlygeek/tabular)

- [majutsushi/tagbar](https://github.com/majutsushi/tagbar)

- [Shougo/neocomplete](https://github.com/Shougo/neocomplete.vim)

- [Shougo/neosnippet](https://github.com/Shougo/neosnippet.vim)

- [Shougo/neosnippet-snippets](https://github.com/Shougo/neosnippet-snippets)

- [honza/vim-snippets](https://github.com/honza/vim-snippets)

- [klen/python-mode](https://github.com/klen/python-mode)

- [yssource/python.vim](https://github.com/yssource/python.vim)

- [python_match.vim](http://www.vim.org/scripts/script.php?script_id=386)

- [pythoncomplete](http://www.vim.org/scripts/script.php?script_id=1542)

- [rodjek/vim-puppet](https://github.com/rodjek/vim-puppet)

- [tpope/vim-markdown](https://github.com/tpope/vim-markdown)

- [vim-scripts/csv.vim](https://github.com/vim-scripts/csv.vim)

### Replaced/removed plugins

- [Lokaltog/vim-easymotion](https://github.com/Lokaltog/vim-easymotion)

## Donate

If you like this project, please donate. You can send coins to the following addresses.

**Bitcoin**: 3KWsKEw3Ewu7vcTREjQUuR8LUf4QXoZEHK

**Litecoin**: MHaqGAqMoQGiTkNyg7w8haC6mU25Frgi3M

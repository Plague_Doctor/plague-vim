#!/usr/bin/env bash

AbadonnaVim_dir="$HOME/.abadonna-vim"

AbadonnaVim_git_url='https://gitlab.com/Abadonna/abadonna-vim.git'

check_deps() {
    if [ -z "$HOME" ]; then
        echo "\$HOME is empty. Please fix before proceeding."
        exit 1
    fi

    command -v vim >/dev/null 2>&1 || { echo >&2 "vim is not installed. Please fix before proceeding."; exit 1; }
    command -v git >/dev/null 2>&1 || { echo >&2 "git is not installed. Please fix before proceeding."; exit 1; }

}

clone_abadonna-vim() {
    if [ ! -e "$AbadonnaVim_dir" ]; then
        echo "Clonning Abadonna-Vim repo..."
        git clone --recursive "$AbadonnaVim_git_url" "$AbadonnaVim_dir"
    else
        echo "Updating Abadonna-vim repo..."
        cd "$AbadonnaVim_dir"
        git pull origin master
    fi
}

install_plugvim() {
    if [ ! -f "$AbadonnaVim_dir/autoload/plug.vim" ]; then
        echo "Install Plug.vim..."
        curl -fLo $AbadonnaVim_dir/autoload/plug.vim --create-dirs \
            https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim
    fi
}

create_structure() {
    echo "Creating Abadonna-vim structure..."
    if [ ! -d "$AbadonnaVim_dir/plugged" ]; then mkdir -p "$AbadonnaVim_dir/plugged"; fi
    if [ ! -L "$HOME/.vim" ]; then ln -sf "$AbadonnaVim_dir" "$HOME/.vim"; fi
    if [ ! -L "$HOME/.vimrc" ]; then ln -sf "$AbadonnaVim_dir/vimrc" "$HOME/.vimrc"; fi
    if [ ! -L "$HOME/.vimrc.plugs" ]; then ln -sf "$AbadonnaVim_dir/vimrc.plugs" "$HOME/.vimrc.plugs"; fi
    if [ ! -L "$HOME/.vimrc.before" ]; then ln -sf "$AbadonnaVim_dir/vimrc.before" "$HOME/.vimrc.before"; fi
}

setup_plugvim() {
    echo "Setup plug.vim and its plugins..."
    system_shell="$SHELL"
    export SHELL='/bin/bash'
    vim -u "$HOME/.vimrc.plugs" \
        "+set nomore" \
        "+PlugInstall!" \
        "+PlugClean!" \
        "+qall"
    export SHELL="$system_shell"
}

 ######################### #MAIN Function ##########################

check_deps
clone_abadonna-vim
create_structure
install_plugvim
setup_plugvim

echo "Abadonna-vim Extension project has been installed. Thank you."

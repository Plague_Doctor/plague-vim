" Release Notes {
"                                                                                           ver. 2.5
"   █████╗ ██████╗  █████╗ ██████╗  ██████╗ ███╗   ██╗███╗   ██╗ █████╗       ██╗   ██╗██╗███╗   ███╗
"  ██╔══██╗██╔══██╗██╔══██╗██╔══██╗██╔═══██╗████╗  ██║████╗  ██║██╔══██╗      ██║   ██║██║████╗ ████║
"  ███████║██████╔╝███████║██║  ██║██║   ██║██╔██╗ ██║██╔██╗ ██║███████║█████╗██║   ██║██║██╔████╔██║
"  ██╔══██║██╔══██╗██╔══██║██║  ██║██║   ██║██║╚██╗██║██║╚██╗██║██╔══██║╚════╝╚██╗ ██╔╝██║██║╚██╔╝██║
"  ██║  ██║██████╔╝██║  ██║██████╔╝╚██████╔╝██║ ╚████║██║ ╚████║██║  ██║       ╚████╔╝ ██║██║ ╚═╝ ██║
"  ╚═╝  ╚═╝╚═════╝ ╚═╝  ╚═╝╚═════╝  ╚═════╝ ╚═╝  ╚═══╝╚═╝  ╚═══╝╚═╝  ╚═╝        ╚═══╝  ╚═╝╚═╝     ╚═╝
"
"       This is my personal .vimrc file. It is a part of Abadonna-Vim Extension Project.
"       Abadonna-Vim Extension is a fork of spf13-vim project.
"
"       The file provides a list of configurations for Vim. While most of the settings modified
"       by this file are highly recommended and very usable, I strongly advise to customise
"       them according to your needs.
" }

" Use before config if available {
    if filereadable(expand("~/.vimrc.before"))
        source ~/.vimrc.before
    endif
" }

" Use pluggeds config {
    if filereadable(expand("~/.vimrc.plugs"))
        source ~/.vimrc.plugs
    endif
" }

" ==========================================================================================

" General {
    set background=dark         " Assume a dark background
    filetype plugin indent on   " Automatically detect file types.
    syntax on                   " Syntax highlighting
    set mouse=a                 " Automatically enable mouse usage
    set mousehide               " Hide the mouse cursor while typing
    set encoding=utf-8          " Set utf-8 as display encoding.
    set fileencoding=utf-8      " Set utf-8 as written to file encoding.

    set viewoptions=folds,options,cursor,unix,slash " Better Unix / Windows compatibility
    set virtualedit=onemore     " Allow for cursor beyond last character
    set history=500             " Store a ton of history (default is 20)
    set nospell                 " Spell checking off
    set hidden                  " Allow buffer switching without saving

    " Setup global behaviour.
    " To disable this, add the following to your .vim.before.local:
    "   let g:abavim_no_global = 1

    if !exists('g:abavim_no_global')
        set gdefault                " Set global as default
    endif

    " Setup persistant backup.
    " To disable this, add the following to your .vim.before.local:
    "   let g:abavim_no_backup = 1

    if !exists('g:abavim_no_backup')
        set backup                  " Backups are nice...
        if has('persistent_undo')
            set undofile                    " Persist undo is even nicer
            set undodir=$HOME/.vim.undo     " Set single dir for undo
            set undolevels=1000             " Maximum number of changes that can be undone
            set undoreload=5000             " Maximum number lines to save for undo on a buffer reload
        endif
    else
        set nobackup                " Do not backup
        set nowritebackup           " Do not save backups
    endif

    " When possible use + register for copy-paste

    if has('clipboard') && has('unnamedplus')
        set clipboard=unnamed,unnamedplus
    endif

    " Most prefer to automatically switch to the current file directory when
    " a new buffer is opened; to prevent this behavior, add the following to
    " your .vimrc.before file:
    "   let g:abavim_no_autochdir = 1

    if !exists('g:abavim_no_autochdir')
        " Always switch to the current file directory
        augroup GroupLocalCD
            autocmd!
            autocmd BufEnter * silent! lcd %:p:h
        augroup END
    endif

    " You can extend end of word designators.
    " Add the following to your .vimrc.before file:
    "   let g:abavim_extend_word_designator = 1

    if !exists('g:abavim_extend_word_designator')
        set iskeyword-=.        " '.' is an end of word designator
        set iskeyword-=#        " '#' is an end of word designator
        set iskeyword-=-        " '-' is an end of word designator
    endif

    " Instead of reverting the cursor to the last position in the buffer, we
    " set it to the first line when editing a git commit message

    au FileType gitcommit au! BufEnter COMMIT_EDITMSG call setpos('.', [0, 1, 1, 0])

    " Restore cursor to file position in previous editing session
    " To disable this, add the following to your .vimrc.before.local file:
    "   let g:abavim_no_restore_cursor = 1

    if !exists('g:abavim_no_restore_cursor')
        function! ResCur()
            if line("'\"") <= line("$")
                normal! g`"
                return 1
            endif
        endfunction

        augroup resCur
            autocmd!
            autocmd BufWinEnter * call ResCur()
        augroup END
    endif

    " Set blowfish encryption. Blowfish is stronger, that's why.

    if v:version >= 703
        set cryptmethod=blowfish
    endif

" }  General

" ==========================================================================================

" Vim UI {

    " Use 256-grayvim color scheme.
    " Other schemes worth to be tried: 256-grayvim, wombat256mod, xoria256

    colorscheme gruvbox
    let g:gruvbox_contrast_dark='hard'

    let g:airline_theme='luna'      " Use Luna as airline theme
    set t_Co=256                    " 256 colors in vim
    set showmode                    " Display the current mode

    " Define color of the CursorLine

    hi CursorLine ctermbg=237 guibg=#3a3a3a cterm=none gui=none
    set cursorline                  " CursorLine On"

    highlight clear SignColumn      " SignColumn should match background
    highlight clear LineNr          " Current line number row will have same background color in relative mode

    " Set permanent universal vertical marker when line is longer that 80
    " characters.
    " To enable this, add the following to your .vimrc.before.local file:
    "   let g:abavim_set_line_marker = 1

    if exists('g:abavim_set_line_marker')
        highlight ColorColumn ctermbg=magenta
        call matchadd('ColorColumn', '\%80v', 100)
    endif

    set backspace=indent,eol,start  " Backspace for dummies
    set linespace=0                 " No extra spaces between rows
    set nu                          " Line numbers on
    set showmatch                   " Show matching brackets/parenthesis
    set matchtime=2                 " Tens of a second to show matchin brackets/parenthesis
    set incsearch                   " Find as you type search
    set hlsearch                    " Highlight search terms
    set winminheight=0              " Windows can be 0 line high
    set ignorecase                  " Case insensitive search
    set smartcase                   " Case sensitive when uc present
    set wildmenu                    " Show list instead of just completing
    set wildmode=list:longest,full  " Command <Tab> completion, list matches, then longest common part, then all.
    set whichwrap=b,s,h,l,<,>,[,]   " Backspace and cursor keys wrap too
    set scrolljump=5                " Lines to scroll when cursor leaves screen
    set scrolloff=3                 " Minimum lines to keep above and below cursor
    set list
    set listchars=tab:»\ ,trail:•,extends:#,nbsp:. " Highlight problematic whitespace

    if has('cmdline_info')
        set ruler                   " Show the ruler
        set rulerformat=%30(%=\:b%n%y%m%r%w\ %l,%c%V\ %P%) " A ruler on steroids
        set showcmd                 " Show partial commands in status line and
                                    " Selected characters/lines in visual mode
    endif

    if has('statusline')
        set laststatus=2

        " Broken down into easily includeable segments

        set statusline=%<%f\                     " Filename
        set statusline+=%w%h%m%r                 " Options
        set statusline+=%{fugitive#statusline()} " Git Hotness
        set statusline+=\ [%{&ff}/%Y]            " Filetype
        set statusline+=\ [%{getcwd()}]          " Current dir
        set statusline+=%=%-14.(%l,%c%V%)\ %p%%  " Right aligned file nav info
    endif

    if filereadable(expand("~/.vim/plugged/vim-colors-solarized/colors/solarized.vim")) && exists('g:abavim_solarized_colors')
        let g:solarized_termcolors=256
        let g:solarized_termtrans=1
        let g:solarized_contrast="normal"
        let g:solarized_visibility="normal"
        color solarized             " Load a colorscheme
    endif

" }  Vim UI

" ==========================================================================================

" Formatting {

    set nowrap                      " Do not wrap long lines
    set autoindent                  " Indent at the same level of the previous line
    set shiftwidth=4                " Use indents of 4 spaces
    set expandtab                   " Tabs are spaces, not tabs
    set tabstop=4                   " An indentation every four columns
    set softtabstop=4               " Let backspace delete indent
    set nojoinspaces                " Prevents inserting two spaces after punctuation on a join (J)
    set splitright                  " Puts new vsplit windows to the right of the current
    set splitbelow                  " Puts new split windows to the bottom of the current
    set pastetoggle=<F12>           " Sane indentation on pastes
    set foldmethod=indent           " Set foldmethod to indent
    set foldlevel=20                " Set default folding level
    set foldlevelstart=20           " As above, but sets when vim gets opened.


    " Remove trailing whitespaces and ^M chars
    " To disable the stripping of whitespace, add the following to your
    " .vimrc.before.local file:
    "   let g:abavim_keep_trailing_whitespace = 1

    if !exists('g:abavim_keep_trailing_whitespace')
        augroup GroupTrailingSpaces
            autocmd!
            autocmd FileType c,cpp,java,go,php,javascript,puppet,python,rust,twig,xml,yml,perl,sql,bash
            \ autocmd BufWritePre <buffer> :call StripTrailingWhitespace()
        augroup END
    endif

    " Automatically opened all folds. {
    augroup GroupOpenAllFolds
        autocmd!
        autocmd BufWinEnter * silent! :%foldopen!
    augroup END
    "}

    " Auto csv tabulate {
    augroup CSV_Editing
        autocmd!
        autocmd BufRead,BufWritePost *.csv :%ArrangeColumn
        autocmd BufWritePre *.csv :%UnArrangeColumn
    augroup END
    "}

" }  Formatting

" ==========================================================================================

" Key (re)Mappings {

    let mapleader = ','

    " F1 shows NERDTree. Who needs Help anyway?

    noremap <F1>                :NERDTreeToggle<CR>
    inoremap <F1>               <esc>:NERDTreeToggle<CR>

    " F8 shows TagBar.

    noremap <F8>                :TagbarToggle<CR>
    inoremap <F8>               <esc>:TagbarToggle<CR>

    " ALT + Left/Right cycles through openned buffers
    " CONTROL + Left/Right/Up/Down cycles through openned windows
    " CONTROL + hjkl does the same

    nnoremap <silent> <A-Left>  :bprevious<CR>
    nnoremap <silent> <A-Right> :bnext<CR>
    nnoremap <C-Left>           :wincmd h<CR>
    nnoremap <C-Right>          :wincmd l<CR>
    nnoremap <C-Up>             :wincmd k<CR>
    nnoremap <C-Down>           :wincmd j<CR>

    nnoremap <C-H>              :wincmd h<CR>
    nnoremap <C-J>              :wincmd j<CR>
    nnoremap <C-K>              :wincmd k<CR>
    nnoremap <C-L>              :wincmd l<CR>

    " ALT + Down drops current buffer

    nnoremap <silent> <A-Down>  :bdelete<CR>

    " F5/F6 show line numbers
    " Shift + F5/F6 hide line numbers

    nnoremap <silent> <F5>      :set number<CR>
    nnoremap <silent> <S-F5>    :set nonumber<CR>
    nnoremap <silent> <F6>      :set relativenumber<CR>
    nnoremap <silent> <S-F6>    :set norelativenumber<CR>

    " Leader + s = toggle nospell

    nnoremap <Leader>s       :set nospell!<CR>

    " Highlight line and column

    nnoremap <Leader>hh      :set cursorline! cursorcolumn!<CR>
    nnoremap <Leader>hl      :set cursorline!<CR>
    nnoremap <Leader>hc      :set cursorcolumn!<CR>

    " Enable/Disable NeoComplCache

    nnoremap <Leader>nc1        :NeoCompleteEnable<CR>
    nnoremap <Leader>nc0        :NeoCompleteDisable<CR>

    " Clear whitespace

    nnoremap <Leader>wd         :%s/\s\+$//<CR>

    " Yank from the cursor to the end of the line, to be consistent with C and D.

    nnoremap Y y$

    " Code folding options

    nnoremap <leader>f0 :set foldlevel=0<CR>
    nnoremap <leader>f1 :set foldlevel=1<CR>
    nnoremap <leader>f2 :set foldlevel=2<CR>
    nnoremap <leader>f3 :set foldlevel=3<CR>
    nnoremap <leader>f4 :set foldlevel=4<CR>
    nnoremap <leader>f5 :set foldlevel=5<CR>
    nnoremap <leader>f6 :set foldlevel=6<CR>
    nnoremap <leader>f7 :set foldlevel=7<CR>
    nnoremap <leader>f8 :set foldlevel=8<CR>
    nnoremap <leader>f9 :set foldlevel=9<CR>

    " BackSpace = no highlight

    nnoremap <silent> <BS> :nohlsearch<CR>

    " Find merge conflict markers

    map <leader>fc /\v^[<\|=>]{7}( .*\|$)<CR>

    " Visual shifting (does not exit Visual mode)

    vnoremap < <gv
    vnoremap > >gv

    " Allow using the repeat operator with a visual selection (!)
    " http://stackoverflow.com/a/8064607/127816

    vnoremap . :normal .<CR>

    " For when you forget to sudo.. Really Write the file.

    cmap w!! w !sudo tee % >/dev/null

    " Easier horizontal scrolling

    map zl zL
    map zh zH

" }  Key (re)Mappings

" ==========================================================================================

" Plugins {

    if isdirectory(expand("~/.vim/plugged/nerdtree"))
        let g:NERDShutUp=1
    endif

    " =====================================================================================

    " NerdTree {
        if isdirectory(expand("~/.vim/plugged/nerdtree"))
            map <C-e> <plug>NERDTreeTabsToggle<CR>
            map <leader>e :NERDTreeFind<CR>
            nmap <leader>nt :NERDTreeFind<CR>

            let NERDTreeShowBookmarks=1
            let NERDTreeIgnore=['\.py[cd]$', '\~$', '\.swo$', '\.swp$', '^\.git$', '^\.hg$', '^\.svn$', '\.bzr$']

            " NERDTreeChDirMode=2 makes NERDTree to work better with CtrlP
            let NERDTreeChDirMode=2
            let NERDTreeQuitOnOpen=1
            let NERDTreeMouseMode=2
            let NERDTreeShowHidden=1
            let NERDTreeKeepTreeInNewTab=1
            let g:nerdtree_tabs_open_on_gui_startup=0

            " Close NERDTree if it is the last open buffer
            augroup GroupNerdTree
                autocmd!
                autocmd WinEnter * call s:CloseIfOnlyNerdTreeLeft()
            augroup END
        endif
    " }

    " =====================================================================================

    " Sneak - a minimalist alternative to EasyMotion {
        if isdirectory(expand("~/.vim/plugged/vim-sneak"))
            let g:sneak#streak = 1
            let g:sneak#s_next = 1

            "replace 'f' with 1-char Sneak
            nmap f <Plug>Sneak_f
            nmap F <Plug>Sneak_F
            xmap f <Plug>Sneak_f
            xmap F <Plug>Sneak_F
            omap f <Plug>Sneak_f
            omap F <Plug>Sneak_F
            "replace 't' with 1-char Sneak
            nmap t <Plug>Sneak_t
            nmap T <Plug>Sneak_T
            xmap t <Plug>Sneak_t
            xmap T <Plug>Sneak_T
            omap t <Plug>Sneak_t
            omap T <Plug>Sneak_T
        endif
    " }

    " =====================================================================================

    " GitGutter {
        if isdirectory(expand("~/.vim/plugged/vim-gitgutter"))
            if !exists('g:abavim_gitgutter_realtime')
                let g:gitgutter_realtime = 0
                let g:gitgutter_eager = 0
            endif
            let g:gitgutter_enabled = 1
            let g:gitgutter_signs = 1
            let g:gitgutter_max_signs = 500
            let g:gitgutter_sign_added = '✚ '
            let g:gitgutter_sign_modified = '≈ '
            let g:gitgutter_sign_removed = '✖ '
            let g:gitgutter_sign_removed_first_line = '✠ '
            let g:gitgutter_sign_modified_removed = '★ '
            nnoremap <silent> <leader>gg :GitGutterToggle<CR>
        endif
    " }

    " =====================================================================================

    " Tabularize {
        if isdirectory(expand("~/.vim/plugged/tabular"))
            nmap <Leader>a& :Tabularize /&<CR>
            vmap <Leader>a& :Tabularize /&<CR>
            nmap <Leader>a= :Tabularize /=<CR>
            vmap <Leader>a= :Tabularize /=<CR>
            nmap <Leader>a=> :Tabularize /=><CR>
            vmap <Leader>a=> :Tabularize /=><CR>
            nmap <Leader>a: :Tabularize /:<CR>
            vmap <Leader>a: :Tabularize /:<CR>
            nmap <Leader>a:: :Tabularize /:\zs<CR>
            vmap <Leader>a:: :Tabularize /:\zs<CR>
            nmap <Leader>a, :Tabularize /,<CR>
            vmap <Leader>a, :Tabularize /,<CR>
            nmap <Leader>a,, :Tabularize /,\zs<CR>
            vmap <Leader>a,, :Tabularize /,\zs<CR>
            nmap <Leader>a<Bar> :Tabularize /<Bar><CR>
            vmap <Leader>a<Bar> :Tabularize /<Bar><CR>
        endif
    " }

    " =====================================================================================

    " Vim-Puppet {
        if isdirectory(expand("~/.vim/plugged/vim-puppet"))
            let g:puppet_align_hashes = 1
            let g:syntastic_puppet_puppetlint_args = "--no-80chars-check --no-autoloader_layout-check"
        endif
    " }

    " =====================================================================================

    " PyMode {
        " Disable if python support not present
        if !has('python')
            let g:pymode = 0
        endif

        if isdirectory(expand("~/.vim/plugged/python-mode"))
            let g:pymode_lint_checkers = ['pyflakes']
            let g:pymode_trim_whitespaces = 1
            let g:pymode_options = 0
            let g:pymode_rope = 0
            let g:pymode_lint_signs = 1
            let g:pymode_lint_todo_symbol = '∴ '
            let g:pymode_lint_comment_symbol = '★ '
            let g:pymode_lint_visual_symbol = '→ '
            let g:pymode_lint_error_symbol = '✗ '
            let g:pymode_lint_info_symbol = '∆ '
            let g:pymode_lint_pyflakes_symbol = '✠ '
        endif
    " }

    " =====================================================================================

    " Syntastic {
        if isdirectory(expand("~/.vim/plugged/syntastic"))
            set statusline+=%#warningmsg#
            set statusline+=%{SyntasticStatuslineFlag()}
            set statusline+=%*
            let g:syntastic_always_populate_loc_list = 1
            let g:syntastic_auto_loc_list = 1
            let g:syntastic_check_on_open = 1
            let g:syntastic_enable_signs = 1
            let g:syntastic_check_on_wq = 0
            let g:syntastic_error_symbol = '✗ '
            let g:syntastic_style_error_symbol = '✠ '
            let g:syntastic_warning_symbol = '∆ '
            let g:syntastic_style_warning_symbol = '≈ '
        endif
    " }

    " =====================================================================================

    " ctrlp {
        if isdirectory(expand("~/.vim/plugged/ctrlp.vim/"))
            let g:ctrlp_working_path_mode = 'ra'
            let g:ctrlp_cmd = 'CtrlPLastMode'
            let g:ctrlp_custom_ignore = {
                \ 'dir':  '\.git$\|\.hg$\|\.svn$',
                \ 'file': '\.exe$\|\.so$\|\.dll$\|\.pyc$' }

            let s:ctrlp_fallback = 'find %s -type f'

            let g:ctrlp_user_command = {
                \ 'types': {
                    \ 1: ['.git', 'cd %s && git ls-files . --cached --exclude-standard --others'],
                    \ 2: ['.hg', 'hg --cwd %s locate -I .'],
                \ },
                \ 'fallback': s:ctrlp_fallback
            \ }

            if isdirectory(expand("~/.vim/plugged/ctrlp-funky/"))
                " CtrlP extensions
                let g:ctrlp_extensions = ['funky', 'mixed', 'line', 'dir']

                "funky
                nnoremap <Leader>fu :CtrlPFunky<Cr>
            endif
        endif
    "}

    " =====================================================================================

    " Fugitive {
        if isdirectory(expand("~/.vim/plugged/vim-fugitive/"))
            nnoremap <silent> <leader>gs :Gstatus<CR>
            nnoremap <silent> <leader>gd :Gdiff<CR>
            nnoremap <silent> <leader>gc :Gcommit<CR>
            nnoremap <silent> <leader>gb :Gblame<CR>
            nnoremap <silent> <leader>gl :Glog<CR>
            nnoremap <silent> <leader>gp :Git push<CR>
            nnoremap <silent> <leader>gr :Gread<CR>
            nnoremap <silent> <leader>gw :Gwrite<CR>
            nnoremap <silent> <leader>ge :Gedit<CR>
            " Mnemonic _i_nteractive
            nnoremap <silent> <leader>gi :Git add -p %<CR>
        endif
    "}

    " =====================================================================================

    " neocomplete {
        " Disable AutoComplPop.
        let g:acp_enableAtStartup = 0
        " Use neocomplete.
        let g:neocomplete#enable_at_startup = 0
        " Use smartcase.
        let g:neocomplete#enable_smart_case = 1
        " Set minimum syntax keyword length.
        let g:neocomplete#sources#syntax#min_keyword_length = 3
        let g:neocomplete#lock_buffer_name_pattern = '\*ku\*'

        " Define dictionary. {
            let g:neocomplete#sources#dictionary#dictionaries = {
                \ 'default' : '',
                \ 'vimshell' : $HOME.'/.vimshell_hist',
                \ 'scheme' : $HOME.'/.gosh_completions'
                    \ }
        "}

        " Define keyword. {
            if !exists('g:neocomplete#keyword_patterns')
                let g:neocomplete#keyword_patterns = {}
            endif
            let g:neocomplete#keyword_patterns['default'] = '\h\w*'
        " }

        " Plugin key-mappings. {
            inoremap <expr><C-g>     neocomplete#undo_completion()
            inoremap <expr><C-l>     neocomplete#complete_common_string()

            " Recommended key-mappings.
            " <CR>: close popup and save indent.
            inoremap <silent> <CR> <C-r>=<SID>my_cr_function()<CR>

            function! s:my_cr_function()
              return (pumvisible() ? "\<C-y>" : "" ) . "\<CR>"
              " For no inserting <CR> key.
              "return pumvisible() ? "\<C-y>" : "\<CR>"
            endfunction

            " <TAB>: completion.
            inoremap <expr><TAB>  pumvisible() ? "\<C-n>" : "\<TAB>"
            " <C-h>, <BS>: close popup and delete backword char.
            inoremap <expr><C-h> neocomplete#smart_close_popup()."\<C-h>"
            inoremap <expr><BS> neocomplete#smart_close_popup()."\<C-h>"
            " Close popup by <Space>.
            "inoremap <expr><Space> pumvisible() ? "\<C-y>" : "\<Space>"
        " }

        " Enable omni completion. {
            autocmd FileType css setlocal omnifunc=csscomplete#CompleteCSS
            autocmd FileType html,markdown setlocal omnifunc=htmlcomplete#CompleteTags
            autocmd FileType javascript setlocal omnifunc=javascriptcomplete#CompleteJS
            autocmd FileType python setlocal omnifunc=pythoncomplete#Complete
            autocmd FileType xml setlocal omnifunc=xmlcomplete#CompleteTags
        " }

        " Enable heavy omni completion. {
            if !exists('g:neocomplete#sources#omni#input_patterns')
              let g:neocomplete#sources#omni#input_patterns = {}
            endif
        " }

        " Plugin NeoSnippet key-mappings. {
            imap <C-k>     <Plug>(neosnippet_expand_or_jump)
            smap <C-k>     <Plug>(neosnippet_expand_or_jump)
            xmap <C-k>     <Plug>(neosnippet_expand_target)
            smap <expr><TAB> neosnippet#expandable_or_jumpable() ?
            \ "\<Plug>(neosnippet_expand_or_jump)" : "\<TAB>"
        " }

    " }

    " =====================================================================================

    " Snippets {
        " Use honza's snippets.
        let g:neosnippet#snippets_directory='~/.vim/plugged/vim-snippets/snippets'

        " Enable neosnippet snipmate compatibility mode
        let g:neosnippet#enable_snipmate_compatibility = 1

        " For snippet_complete marker.
        if has('conceal')
            set conceallevel=2 concealcursor=i
        endif

        " Enable neosnippets when using go
        let g:go_snippet_engine = "neosnippet"

        " Disable the neosnippet preview candidate window
        " When enabled, there can be too much visual noise
        " especially when splits are used.
        set completeopt-=preview
    " }

    " =====================================================================================

    " UndoTree {
        if isdirectory(expand("~/.vim/plugged/undotree/"))
            nnoremap <Leader>u :UndotreeToggle<CR>
            " If undotree is opened, it is likely one wants to interact with it.
            let g:undotree_SetFocusWhenToggle=1
        endif
    " }

    " =====================================================================================

    " Wildfire {
    let g:wildfire_objects = {
                \ "*" : ["i'", 'i"', "i)", "i]", "i}", "ip"],
                \ "html,xml" : ["at"],
                \ }
    " }

    " =====================================================================================

    " vim-airline {
        " Set configuration options for the statusline plugin vim-airline.
        " Use the powerline theme and optionally enable powerline symbols.
        " To use the symbols , , , , , , and .in the statusline
        " segments add the following to your .vimrc.before file:
        "   let g:airline_powerline_fonts=1
        " If the previous symbols do not render for you then install a
        " powerline enabled font.

        " See `:echo g:airline_theme_map` for some more choices
        " Default in terminal vim is 'dark'
        if isdirectory(expand("~/.vim/plugged/vim-airline/"))
            if !exists('g:abavim_hide_tabs')
                let g:airline#extensions#tabline#enabled = 1
                let g:airline#extensions#tabline#left_sep = ' '
                let g:airline#extensions#tabline#left_alt_sep = '|'
            endif
            if !exists('g:airline_theme')
                let g:airline_theme = 'solarized'
            endif
            if !exists('g:airline_powerline_fonts')
                " Use the default set of separators with a few customizations
                let g:airline_left_sep='›'  " Slightly fancier than '>'
                let g:airline_right_sep='‹' " Slightly fancier than '<'
            endif
        endif
    " }

" }  Plugins

" ==========================================================================================

" Functions {

    " Initialize directories {
    function! InitializeDirectories()
        let parent = $HOME
        let prefix = 'vim.'
        let common_dir = parent . '/.' . prefix
        let dir_list = {
        \ 'backup': 'backupdir'}

        if has('persistent_undo')
            let dir_list['undo'] = 'undodir'
        endif

        for [dirname, settingname] in items(dir_list)
            let directory = common_dir . dirname . '/'
            if exists("*mkdir")
                if !isdirectory(directory)
                    call mkdir(directory)
                endif
            endif

            if !isdirectory(directory)
                echo "Warning: Unable to create backup directory: " . directory
                echo "Try: mkdir -p " . directory
            else
                let directory = substitute(directory, " ", "\\\\ ", "g")
                exec "set " . settingname . "=" . directory
            endif
        endfor
    endfunction

    call InitializeDirectories()
    " }

    " Initialize NERDTree as needed {
    function! NERDTreeInitAsNeeded()
        redir => bufoutput
        buffers!
        redir END
        let idx = stridx(bufoutput, "NERD_tree")
        if idx > -1
            NERDTreeMirror
            NERDTreeFind
            wincmd l
        endif
    endfunction
    " }

    " Strip whitespace {
    function! StripTrailingWhitespace()
        " Preparation: save last search, and cursor position.
        let _s=@/
        let l = line(".")
        let c = col(".")
        " do the business:
        %s/\s\+$//e
        " clean up: restore previous search history, and cursor position
        let @/=_s
        call cursor(l, c)
    endfunction
    " }

    " Close NERDTree if it is the last open buffer {
    function! s:CloseIfOnlyNerdTreeLeft()
        if exists("t:NERDTreeBufName")
            if bufwinnr(t:NERDTreeBufName) != -1
                if winnr("$") == 1
                    q
                endif
            endif
        endif
    endfunction
    " }

" }  Functions

" ==========================================================================================

" Use local vimrc if available {
    if filereadable(expand("~/.vimrc.local"))
        source ~/.vimrc.local
    endif
" }
